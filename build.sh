#!/usr/bin/env bash
docker build -t rust-buildchain:v0.0 -f builder/Dockerfile builder
docker build -t bliss/cassandra:3.11 -f services/cassandra/Dockerfile services/cassandra
