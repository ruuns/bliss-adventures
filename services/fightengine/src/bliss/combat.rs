use bliss::actions::{Action, ActionFailure, ActionResult};
use bliss::heros::Hero;
use bliss::maps::Point2D;
use bliss::maps::GeoMap; 
use bliss::dice::{Dice, GameMaster};

const WOUNDS_TO_BE_KILLED : i32 = 3;

pub type Fraction = i32;

#[derive(Clone)]
pub struct Participant {
    hero : Box<Hero>,
    position : Point2D,
    fraction : Fraction,
    wounds : i32,
    movement : i32,
    attacks : i32
}

#[derive(Clone)]
pub struct Battlefield {
    map : Box<GeoMap>,
    participants : Vec<Participant>,
    order : Vec<usize>,
    participant_on_turn : usize,
    turn_number : i32,
}

impl Participant {
    fn new(hero: Box<Hero>, position: Point2D, fraction: Fraction) -> Participant {
        let mut p = Participant { hero, position, fraction, wounds: 0, movement: 0, attacks: 0 };
        p.reset_movement();
        p
    }

    pub fn hero(&self) -> &Hero {
        &self.hero
    }

    pub fn attacks(&self) -> i32 {
        self.attacks
    }

    pub fn fraction(&self) -> Fraction {
        self.fraction
    }

    pub fn initiative(&self) -> i32 {
        self.hero.initiative()
    }

    pub fn regenerate(&mut self) {
        self.reset_attack();
        self.reset_movement()
    }

    pub fn movement(&self) -> i32 {
        self.movement
    }

    pub fn reset_movement(&mut self) {
        let movement_bonus = self.hero.movement();
        self.movement = 5 + movement_bonus
    }

    pub fn update_movement(&mut self, new_movement: i32) {
        self.movement = new_movement
    }

    pub fn reset_attack(&mut self) {
        self.attacks = 0
    }

    pub fn add_attack(&mut self, attacks: i32) {
        self.attacks += attacks
    }

    pub fn is_alive(&self) -> bool {
        self.wounds < WOUNDS_TO_BE_KILLED
    }

    pub fn kill(&mut self) {
        self.hurt(WOUNDS_TO_BE_KILLED)
    }

    pub fn hurt(&mut self, wounds: i32) {
        self.wounds += wounds
    }

    pub fn wounds(&self) -> i32 {
        self.wounds
    }

    pub fn position(&self) -> Point2D {
        self.position.clone()
    }

    pub fn update_position(&mut self, point: Point2D) {
        self.position = point
    }

    pub fn weapon_rating_bonus(&self) -> i32 {
        self.hero().equipment().weapon_rating_bonus()
    }

    pub fn armory_rating_bonus(&self) -> i32 {
        self.hero().equipment().armory_rating_bonus()
    }

    pub fn fighting(&self) -> i32 {
        self.hero().fighting()
    }

    pub fn defence(&self) -> i32 {
        self.hero().defence()
    }

    pub fn attack_range(&self) -> i32 {
        self.hero().equipment().range()
    }
}

fn current_participant_index(order: &Vec<usize>, pos: usize) -> usize {
    order[pos]
}

impl Battlefield {
    pub fn new(map: Box<GeoMap>, num_heros: usize) -> Battlefield {
        Battlefield { map, 
            participants: Vec::with_capacity(num_heros), 
            order : Vec::with_capacity(num_heros),
            participant_on_turn: 0,
            turn_number: 0
        }
    }

    pub fn add_participant(&mut self, hero: Box<Hero>, start: Point2D, fraction: i32) {
        self.order.push(self.participants.len());
        self.participants.push(Participant::new(hero, start, fraction))
    }

    pub fn participants(&self) -> &Vec<Participant> {
        &self.participants
    }

    pub fn mut_participants(&mut self) -> &mut Vec<Participant> {
        &mut self.participants
    }

    pub fn current_turn(&self) -> i32 {
        self.turn_number
    }

    pub fn number_of_participants(&self) -> usize {
        self.participants.len()
    }

    pub fn map(&self) -> &GeoMap {
        &*self.map
    }

    pub fn set_order(&mut self, order: Vec<usize>) {
        if order.len() != self.number_of_participants() {
            panic!("Fixed order length does not matches the number of participants")
        }
        self.order = order;
    }

    pub fn winner(&self) -> Option<Fraction> {
        if !self.participants.iter().any(|p| p.is_alive()) {
            panic!("No player alive in this game. Invalid state")
        }

        let mut found = None;
        for p in self.participants.iter() {
            if p.is_alive() {
                match found {
                    None                            => found = Some(p.fraction()),
                    Some(f) if f == p.fraction()    => found = Some(f),
                    Some(_)                         => return None,
                }
            } 
        };
        return found
    }

    pub fn position_occupied_by_participant(&self, position: Point2D) -> bool {
        self.participants.iter().any(|p| p.position() == position)
    }

    pub fn current_participant(&self) -> &Participant {
        &self.participants[current_participant_index(&self.order, self.participant_on_turn)]
    }

    pub fn current_participant_id(&self) -> usize {
        current_participant_index(&self.order, self.participant_on_turn)
    }
    
    pub fn current_mut_participant(&mut self) -> &mut Participant {
        &mut self.participants[current_participant_index(&self.order, self.participant_on_turn)]
    }

    pub fn run<T : Action>(&mut self, gm: &mut GameMaster, action: T) -> ActionResult {
        action.execute(gm, self)
    }

    pub fn switch_to_next_alive_participant(&mut self, initial_participant: usize) {
        self.participant_on_turn = (self.participant_on_turn + 1) % self.participants.len();
        if current_participant_index(&self.order, self.participant_on_turn) == initial_participant {
            panic!("Only one player is alive on the battlefield. Fight should be already quited")
        };

        if self.participant_on_turn == 0 {
            self.turn_number += 1
        };

        let index = self.order[self.participant_on_turn];
        if !self.participants[index].is_alive() {
            self.switch_to_next_alive_participant(initial_participant)
        };
    }
}

