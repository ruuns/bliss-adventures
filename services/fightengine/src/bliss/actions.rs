use bliss::combat::{Battlefield, Participant};
use bliss::dice::{Dice, GameMaster};
use bliss::maps::{Point2D, manhattan_dist};

type TaskCheck = i32;

#[derive(Debug)]
pub enum ActionCode {
    InvalidArguments,
    RuleViolation
}

#[derive(Debug)]
pub struct ActionFailure {
    code: ActionCode,
    message: String,
}

pub type ActionResult = Result<(), ActionFailure>;

pub fn action_failure<T>(code: ActionCode, message: String) -> Result<T, ActionFailure> {
    Err(ActionFailure { code, message })
}

pub fn invalid_arguments<T>(message: String) -> Result<T, ActionFailure> {
    action_failure(ActionCode::InvalidArguments, message)
}

pub fn rule_violation<T>(message: String) -> Result<T, ActionFailure> {
    action_failure(ActionCode::RuleViolation, message)
}

pub trait Action {
    fn execute(&self, &mut GameMaster, &mut Battlefield) -> ActionResult;
}

// --------------------------------------------------------------------------------------------
// Initiative Phase
// --------------------------------------------------------------------------------------------

pub struct InitiativeReordering {
    fixed_order: Vec<usize>,
}

impl InitiativeReordering {
    pub fn new() -> InitiativeReordering {
        InitiativeReordering { fixed_order: Vec::new() }
    }

    pub fn from_fixed_order(order: &[usize]) -> InitiativeReordering {
        let mut action = InitiativeReordering::new();
        for pos in order.iter() {
            action.fixed_order.push(*pos)
        }
        action
    }

    pub fn throw_dices(&self) -> bool {
        self.fixed_order.is_empty()
    }
}

fn draw_initiative_taskcheck(gm: &mut GameMaster, participant: &Participant) -> TaskCheck {
    let eyes = gm.draw(Dice(1, 20));
    let modifier = participant.initiative();
    eyes + modifier
}

fn run_initiative_throws(gm: &mut GameMaster, participants: &Vec<Participant>) -> Vec<usize> {
    let mut checks = Vec::with_capacity(participants.len());
    // determine a taskcheck for each participant
    for (i, p) in participants.iter().enumerate() {
        let eyes = draw_initiative_taskcheck(gm, p);
        checks.push((i, eyes))
    }
    //  Sort taskchecks and assign new order to current battlefield
    checks.sort_by(|(_,c),(_,d)| c.cmp(d));
    checks.iter().map(|x| x.0).collect()
}

impl Action for InitiativeReordering {
    fn execute(&self, gm: &mut GameMaster, battlefield: &mut Battlefield) -> ActionResult {
        if !self.throw_dices() {
            battlefield.set_order(self.fixed_order.clone())
        } else {
            let p = run_initiative_throws(gm, battlefield.participants());
            battlefield.set_order(p)
        }
        Ok(())
    }
}

// --------------------------------------------------------------------------------------------
// Finsh Turn for a current participant 
// --------------------------------------------------------------------------------------------

pub struct FinishTurn {}

impl FinishTurn {
    pub fn new() -> FinishTurn {
        FinishTurn {}
    }
}

impl Action for FinishTurn {
    fn execute(&self, gm: &mut GameMaster, battlefield: &mut Battlefield) -> ActionResult {
        let current_participant_id = battlefield.current_participant_id();
        battlefield.switch_to_next_alive_participant(current_participant_id);
        battlefield.current_mut_participant().regenerate();
        Ok(())
    }
}

// --------------------------------------------------------------------------------------------
// Move current participant to another position 
// --------------------------------------------------------------------------------------------

pub struct MoveParticipant {
    commands: Vec<Point2D>
}

impl MoveParticipant {
    pub fn new(commands: Vec<Point2D>) -> MoveParticipant {
        MoveParticipant { commands }
    }
    
    pub const LEFT  : Point2D = Point2D(-1,  0);
    pub const RIGHT : Point2D = Point2D( 1,  0);
    pub const DOWN  : Point2D = Point2D( 0, -1);
    pub const UP    : Point2D = Point2D( 0,  1);
}

fn verify_movement_of_current_participant(battlefield: &Battlefield, commands: &Vec<Point2D>) -> Result<(Point2D, i32), ActionFailure> {
    let participant = battlefield.current_participant();
    let mut new_position = participant.position();
    let mut movement_necessary = 0;
    let map = battlefield.map();
    
    for delta in commands.iter() {
        let p = new_position.clone() + delta.clone();
        match map.get(p.clone()) {
            None => 
                return rule_violation(String::from("Participant is moved out of the map")),
            Some(ref cell) if !cell.is_free() =>
                return rule_violation(String::from("Participant is sent to a not movable cell on this movement path")),
            Some(_) => 
                ()
        }
        new_position = p;
        movement_necessary += 1;
    }

    if battlefield.position_occupied_by_participant(new_position.clone()) {
        return rule_violation(String::from("Participant can not move to an occupied location"))
    }

    if movement_necessary > participant.movement() {
        return rule_violation(String::from("Participant has not enough movement points"))
    }
    Ok((new_position, participant.movement() - movement_necessary))
}

fn move_current_participant(battlefield: &mut Battlefield, new_position: Point2D, new_movement: i32) {
    let ref mut participant = battlefield.current_mut_participant();
    participant.update_movement(new_movement);
    participant.update_position(new_position);
}

impl Action for MoveParticipant {
    fn execute(&self, gm: &mut GameMaster, battlefield: &mut Battlefield) -> ActionResult {
        let (new_position, new_movement) = verify_movement_of_current_participant(battlefield, &self.commands)?;
        move_current_participant(battlefield, new_position, new_movement);
        Ok(())
    }
}

// --------------------------------------------------------------------------------------------
// Attack Action  
// --------------------------------------------------------------------------------------------

pub struct Attack {
    target: Point2D,
    attack_taskcheck: Option<i32>,
    touchness_taskcheck: Option<i32>,
}

impl Attack {
    pub fn new(target: Point2D) -> Attack {
        Attack { target, attack_taskcheck: None, touchness_taskcheck: None }
    }

    pub fn without_toughness_check(target: Point2D) -> Attack {
        Attack { target, attack_taskcheck: None, touchness_taskcheck: Some(1) }
    }
}

fn find_target_participant(participants: &Vec<Participant>, target: &Point2D) -> Result<usize, ActionFailure> {
    for (i, p) in participants.iter().enumerate() {
        if p.position() == *target {
            return Ok(i)
        }
    }
    invalid_arguments(String::from("Could not find any participants on given target position"))
}

fn verify_target_is_in_range(current: &Participant, target: &Participant) -> Result<i32, ActionFailure> {
    let range = current.attack_range();
    let distance = manhattan_dist(current.position().clone(), target.position().clone());
    if distance > range {
        return rule_violation(String::from("Target participant is not in range of the weapon"))
    } 
    Ok(distance)
}

fn verify_attack_precondition_of_current_participant(battlefield: &Battlefield, target: &Point2D) -> Result<usize, ActionFailure> {
    if battlefield.current_participant().attacks() >= 1 {
        return rule_violation(String::from("Current participant can only attack once"))
    }
    let target_id = find_target_participant(battlefield.participants(), target)?;
    let _ = verify_target_is_in_range(battlefield.current_participant(), &battlefield.participants()[target_id])?;
    Ok(target_id)
}

fn throw_attack_taskcheck(gm: &mut GameMaster, battlefield: &Battlefield, target: usize) -> Result<i32, ActionFailure> {
    let ref target_participant = battlefield.participants()[target];
    let ref current_participant = battlefield.current_participant();
    let natural_roll = gm.draw(Dice(1,20));

    let weapon_attack_bonus = current_participant.weapon_rating_bonus();
    let fighting_skill = current_participant.fighting();
    
    let armory_defence_bonus = target_participant.armory_rating_bonus();
    let defence_skill = target_participant.defence();

    let attack_bonus = weapon_attack_bonus + fighting_skill;
    let defence_bonus = armory_defence_bonus + defence_skill;
    let wounds = match natural_roll {
        1       => 0,
        20      => 1,
        roll if roll + attack_bonus >= 15 + defence_bonus => 1,
        _       => 0,
    };
    Ok(wounds)
}

fn hurt_participant(battlefield: &mut Battlefield, wound: i32, target_id: usize) {
    let ref mut participants = battlefield.mut_participants();
    let ref mut participant = participants[target_id];
    participant.hurt(wound);
}

impl Action for Attack {
    fn execute(&self, gm: &mut GameMaster, battlefield: &mut Battlefield) -> ActionResult {
        let id = verify_attack_precondition_of_current_participant(battlefield, &self.target)?;
        let wounds = throw_attack_taskcheck(gm, battlefield, id)?;
        hurt_participant(battlefield, wounds, id);
        battlefield.current_mut_participant().add_attack(1);
        Ok(())
    }
}


#[cfg(test)]
mod unittests {
    use bliss::actions::{InitiativeReordering, FinishTurn, MoveParticipant, Attack};
    use bliss::combat::{Battlefield, Fraction};
    use bliss::dice::{GameMaster, GameStrategy};
    use bliss::heros::{Hero, Race, Profession};
    use bliss::maps::GeoMap;
    use bliss::maps::Point2D;

    fn assert_turn(gm: &mut GameMaster, bf: &mut Battlefield, turn: i32, name: &'static str, winner: Option<Fraction>, kill: bool) {
        assert!(bf.current_participant().hero().name() == name);
        assert!(bf.current_turn() == turn);
        assert!(bf.winner() == winner);
        if kill {
            bf.current_mut_participant().kill()
        };
        bf.run(gm, FinishTurn::new()).expect("FinishTurn")
    }

    #[test]
    fn test_participant_order_switching() {
        let names = ["Fritz", "Heinz", "Anna", "Maik"];
        let map = Box::new(GeoMap::new(10,10));
        let mut gm = GameMaster::new();
        let mut bf = Battlefield::new(map, 5);
        let mut heros: Vec<Box<Hero>> = names.iter().map(|name| Box::new(Hero::new(String::from(*name), 30, Profession::Fighter, Race::Human))).collect(); 

        bf.add_participant(heros.remove(0), Point2D(0, 0), 1);
        bf.add_participant(heros.remove(0), Point2D(2, 2), 1);
        bf.add_participant(heros.remove(0), Point2D(3, 3), 2);
        bf.add_participant(heros.remove(0), Point2D(4, 4), 2);

        bf.run(&mut gm, InitiativeReordering::from_fixed_order(&[3, 2, 0, 1]))
            .expect("InitiativeReordering");

        assert_turn(&mut gm, &mut bf, 0, "Maik", None, true);
        assert_turn(&mut gm, &mut bf, 0, "Anna", None, false);
        assert_turn(&mut gm, &mut bf, 0, "Fritz", None, false);
        assert_turn(&mut gm, &mut bf, 0, "Heinz", None, true);
        assert_turn(&mut gm, &mut bf, 1, "Anna", None, false);
        assert_turn(&mut gm, &mut bf, 1, "Fritz", None, true);
        assert!(bf.winner().unwrap() == 2)
    }

    #[test]
    fn test_participant_movement() {
        let map = Box::new(GeoMap::new(10,10));
        let mut gm = GameMaster::new();
        let mut bf = Battlefield::new(map, 5);
        bf.add_participant(Box::new(Hero::new(String::from("Fritz"), 30, Profession::Fighter, Race::Human)), Point2D(0,0), 1);
        bf.add_participant(Box::new(Hero::new(String::from("Heinz"), 30, Profession::Fighter, Race::Human)), Point2D(1,1), 2);
        assert!(bf.current_participant().hero().name() == "Fritz");
        assert!(bf.current_participant().movement() == 5);

        bf.run(&mut gm, MoveParticipant::new(vec![MoveParticipant::RIGHT, MoveParticipant::RIGHT, MoveParticipant::UP]))
            .expect("MoveParticipant");
        assert!(bf.current_participant().position() == Point2D(2,1));
        assert!(bf.current_participant().movement() == 2);

        // Participant should not move out of the map
        bf.run(&mut gm, MoveParticipant::new(vec![MoveParticipant::DOWN, MoveParticipant::DOWN]))
            .expect_err("MoveParticipant Out of the map");
        assert!(bf.current_participant().position() == Point2D(2,1));
        assert!(bf.current_participant().movement() == 2);
 
        // In a single cell only one participant is allowed to stay 
        bf.run(&mut gm, MoveParticipant::new(vec![MoveParticipant::LEFT]))
            .expect_err("MoveParticipant to an occupied cell");
        assert!(bf.current_participant().position() == Point2D(2,1));
        assert!(bf.current_participant().movement() == 2);
 
        // Participants move range are limited by the number of movement points 
        bf.run(&mut gm, MoveParticipant::new(vec![MoveParticipant::RIGHT, MoveParticipant::RIGHT, MoveParticipant::UP]))
            .expect_err("MoveParticipant can only move by the number of participant's movement points");
        assert!(bf.current_participant().position() == Point2D(2,1));
        assert!(bf.current_participant().movement() == 2);
        
        bf.run(&mut gm, FinishTurn::new()).expect("FinishTurn"); // Finish turn for hero 'Fritz'
        bf.run(&mut gm, FinishTurn::new()).expect("FinishTurn"); // Finish turn for hero 'Heinz'

        assert!(bf.current_participant().hero().name() == "Fritz");
        assert!(bf.current_participant().position() == Point2D(2,1));
        assert!(bf.current_participant().movement() == 5);
    }


    #[test]
    fn test_participant_attacks() {
        let map = Box::new(GeoMap::new(10,10));
        let mut gm = GameMaster::with_strategy(GameStrategy::Maximum);
        let mut bf = Battlefield::new(map, 5);

        bf.add_participant(Box::new(Hero::new(String::from("Fritz"), 30, Profession::Fighter, Race::Human)), Point2D(0,0), 1);
        bf.add_participant(Box::new(Hero::new(String::from("Heinz"), 30, Profession::Fighter, Race::Human)), Point2D(0,1), 2);
        assert!(bf.current_participant().hero().name() == "Fritz");

        bf.run(&mut gm, Attack::without_toughness_check(Point2D(0,1))).expect("Attack should hit");
        bf.run(&mut gm, FinishTurn::new()).expect("FinishTurn"); // Finish turn for hero 'Fritz'
        assert!(bf.current_participant().wounds() == 1);
        
        bf.run(&mut gm, Attack::without_toughness_check(Point2D(0,0))).expect("Attack should hit");
        bf.run(&mut gm, FinishTurn::new()).expect("FinishTurn"); // Finish turn for hero 'Fritz'
        assert!(bf.current_participant().wounds() == 1);

        bf.run(&mut gm, Attack::without_toughness_check(Point2D(0,1))).expect("Attack should hit");
        bf.run(&mut gm, FinishTurn::new()).expect("FinishTurn"); // Finish turn for hero 'Fritz'
        assert!(bf.current_participant().wounds() == 2);
 
        bf.run(&mut gm, Attack::without_toughness_check(Point2D(0,0))).expect("Attack should hit");
        bf.run(&mut gm, FinishTurn::new()).expect("FinishTurn"); // Finish turn for hero 'Fritz'
        assert!(bf.current_participant().wounds() == 2);
 
        bf.run(&mut gm, Attack::without_toughness_check(Point2D(0,1))).expect("Attack should hit");
        assert!(bf.winner().unwrap() == 1)
    }

    #[test]
    fn test_invalid_participant_attacks() {
        let map = Box::new(GeoMap::new(10,10));
        let mut gm = GameMaster::with_strategy(GameStrategy::Maximum);
        let mut bf = Battlefield::new(map, 5);

        bf.add_participant(Box::new(Hero::new(String::from("Fritz"), 30, Profession::Fighter, Race::Human)), Point2D(1,2), 1);
        bf.add_participant(Box::new(Hero::new(String::from("Heinz"), 30, Profession::Fighter, Race::Human)), Point2D(0,2), 2);
        assert!(bf.current_participant().hero().name() == "Fritz");
        assert!(bf.current_participant().position() == Point2D(1,2));
        
        bf.run(&mut gm, Attack::without_toughness_check(Point2D(0,2))).expect("Attack should hit");
        bf.run(&mut gm, Attack::without_toughness_check(Point2D(0,2))).expect_err("Only one attack is allowed");
        bf.run(&mut gm, FinishTurn::new()).expect("FinishTurn"); // Finish turn for hero 'Fritz'

        bf.run(&mut gm, MoveParticipant::new(vec![MoveParticipant::UP])).expect("MoveParticipant");
        bf.run(&mut gm, FinishTurn::new()).expect("FinishTurn"); // Finish turn for hero 'Fritz'
        
        bf.run(&mut gm, Attack::without_toughness_check(Point2D(0,2))).expect_err("No player available on position 0,2");
        bf.run(&mut gm, Attack::without_toughness_check(Point2D(0,3))).expect_err("Target is out of range");
    }
}
