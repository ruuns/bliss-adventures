use bliss::combat::Participant;

bitflags! {
    struct Ground : u16 {
        const FREE          = 1 << 0;   // a participant can move to this cell
        const SWIMMING_ONLY = 1 << 1;   // a participant have to swim on this cell which requires other abilities
        const CURSED        = 1 << 2;   // this cell is cursed and will provide a negative effect
        const BLESSED       = 1 << 3;   // this cell is blessed and will provide a positive effect
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct Point2D(pub i32, pub i32);

impl std::ops::Add for Point2D {
    type Output = Point2D;
    fn add(self, other: Point2D) -> Point2D {
        Point2D(self.0 + other.0, self.1 + other.1)
    }
}

pub fn manhattan_dist(x: Point2D, y: Point2D) -> i32 {
    (x.0 - y.0).abs() + (x.1 - y.1).abs() 
}

#[derive(Clone)]
pub struct Cell {
    height: i16,
    ground: Ground
}

impl Cell {
    pub fn free(height: i16) -> Cell {
        Cell { height: 0, ground: Ground::FREE }
    }

    pub fn is_free(&self) -> bool {
        !(self.ground & Ground::FREE).is_empty()
    }
}

#[derive(Clone)]
pub struct GeoMap {
    width: i32,
    height: i32,
    points: Vec<Cell>,
}

impl GeoMap {
    pub fn new(width: i32, height: i32) -> GeoMap {
        GeoMap { width, height, points: vec![Cell::free(0); (width * height) as usize] }
    }

    pub fn get(&self, position: Point2D) -> Option<Cell> {
        let x = position.0;
        let y = position.1;
        let index = (x + y * self.width) as usize;
        if index < self.points.len() { 
            Some(self.points[index].clone()) 
        } else { 
            None 
        }
    }
}
