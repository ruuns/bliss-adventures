
#[derive(Clone)]
pub enum Weight {
    Light, 
    Medium,
    Heavy,
    VeryHeavy
}

#[derive(Clone)]
pub struct Weapon {
    name            : String,
    efficiency      : i32,
    necessary_hands : i32,
    range           : i32, 
    weight          : Weight,
}

impl Weapon {
    fn new(name: String, efficiency: i32, necessary_hands: i32, range: i32, weight: Weight) -> Weapon {
        Weapon { name, efficiency, necessary_hands, range, weight }
    }
}

#[derive(Clone)]
pub struct Armory {
    name        : String,
    efficiency  : i32,
    weight      : Weight,
}

impl Armory {
    fn new(name: String, efficiency: i32, weight: Weight) -> Armory {
        Armory { name, efficiency, weight }
    }
}

#[derive(Clone)]
pub struct Equipment {
    first_weapon    : Option<Weapon>,
    second_weapon   : Option<Weapon>,
    shield          : Option<Armory>,
    armory          : Option<Armory>,
}

impl Equipment {
    pub fn naked_equipment() -> Equipment {
        Equipment { 
            first_weapon: None,
            second_weapon: None,
            shield: None,
            armory: None
        }
    }

    pub fn new(first_weapon: Option<Weapon>, 
               second_weapon: Option<Weapon>, 
               shield: Option<Armory>, 
               armory: Option<Armory>) -> Equipment {
        Equipment { first_weapon, second_weapon, shield, armory }
    }

    pub fn primary_weapon(&self) -> Option<&Weapon> {
        self.first_weapon.as_ref()
    }

    pub fn second_weapon(&self) -> Option<&Weapon> {
        self.second_weapon.as_ref()
    }

    pub fn shield(&self) -> Option<&Armory> {
        self.shield.as_ref()
    }

    pub fn armory(&self) -> Option<&Armory> {
        self.armory.as_ref()
    }

    pub fn weapon_rating_bonus(&self) -> i32 {
        self.first_weapon.as_ref().map_or(0, |weapon| weapon.efficiency)
    }

    pub fn armory_rating_bonus(&self) -> i32 {
        let armory_bonus = self.armory.as_ref().map_or(0, |armory| armory.efficiency);
        let shield_bonus = self.shield.as_ref().map_or(0, |shield| shield.efficiency);
        armory_bonus + shield_bonus
    }

    pub fn range(&self) -> i32 {
        self.first_weapon.as_ref().map_or(1, |weapon| weapon.range)
    }
}
