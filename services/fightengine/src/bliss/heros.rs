use bliss::equipment::{Equipment};

#[derive(Clone)]
pub enum Profession {
    Fighter,
}

#[derive(Clone)]
pub enum Race {
    Human,
}

#[derive(Clone)]
pub struct Attributes {
    // mental attributes
    strength: i32,
    dexterity: i32,
    agility: i32,
    intellect: i32,
    spirit: i32,

    // skill attributes
    defence: i32,
    fighting: i32,
    knowledge: i32,
    mobility: i32,
    shooting: i32,
    toughness: i32,
}

#[derive(Clone)]
pub struct Hero {
    name: String,
    age: i32,
    profession: Profession,
    race: Race,
    attributes: Attributes,
    equipment: Equipment
}

impl Hero {
    pub fn new(name: String, age: i32, profession: Profession, race: Race) -> Self {
        Hero { 
            name, 
            age, 
            profession, 
            race, 
            attributes: Attributes { 
                strength: 0, 
                dexterity: 0, 
                agility: 0, 
                intellect: 0, 
                spirit: 0,  
                defence: 0, 
                fighting: 0, 
                knowledge: 0, 
                mobility: 0, 
                shooting: 0, 
                toughness: 0 
            },
            equipment: Equipment::naked_equipment()
        }
    }

    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn initiative(&self) -> i32 {
        0
    }

    pub fn movement(&self) -> i32 {
        0
    }

    pub fn equipment(&self) -> &Equipment {
        &self.equipment
    }

    pub fn fighting(&self) -> i32 {
        self.attributes.fighting
    }

    pub fn defence(&self) -> i32 {
        self.attributes.defence
    }
}

