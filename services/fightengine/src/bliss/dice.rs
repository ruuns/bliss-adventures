use rand::{Rng, thread_rng};
use rand::rngs::{ThreadRng};
use rand::distributions::Uniform;

pub struct Dice(pub i32, pub i32);

#[derive(Clone)]
pub enum GameStrategy {
    Uniform,
    Maximum,
    Minimum,
}

#[derive(Clone)]
pub struct GameMaster {
    rng : ThreadRng,
    strategy : GameStrategy,
}

impl GameMaster {
    pub fn with_strategy(strategy: GameStrategy) -> GameMaster {
        GameMaster { rng: thread_rng(), strategy: strategy }
    }

    pub fn new() -> GameMaster {
        GameMaster::with_strategy(GameStrategy::Uniform)
    }

    pub fn draw(&mut self, dice: Dice) -> i32 {
        let Dice(throws, eyes) = dice;
        let dist = match self.strategy {
            GameStrategy::Uniform => Uniform::new_inclusive(1, eyes),
            GameStrategy::Maximum => Uniform::new_inclusive(eyes, eyes),
            GameStrategy::Minimum => Uniform::new_inclusive(1, 1),
        };
        (0..throws).map(|_| self.rng.sample(dist)).sum()
    }
}


#[cfg(test)]
mod unittests {
    use super::*;

    #[test]
    fn test_throwing_a_dice() {
        let mut gm = GameMaster::new();
        for _ in 0..100 {
            let v = gm.draw(Dice(1, 20));
            assert!(1 <= v && v <= 20);
        }
    }

    #[test]
    fn test_throwing_several_dices() {
        let mut gm = GameMaster::new();
        for _ in 0..100 {
            let v = gm.draw(Dice(3, 6));
            assert!(3 <= v && v <= 18);
        }
    }


}
