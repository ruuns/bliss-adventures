use std::collections::HashMap;
use std::collections::hash_map::DefaultHasher;
use std::time::{SystemTime};
use std::hash::{Hash, Hasher};
use bliss_protos::fightengine::{RegisterRequest, RegisterResponse};
use bliss_protos::fightengine_grpc::{FightEngineService};
use bliss_protos::code::{Code};
use bliss_protos::status::{Status};
use bliss::combat::{Battlefield};
use bliss::maps::{GeoMap};
use bliss::dice::{GameMaster};

type SessionId = u64;

#[derive(Clone)]
pub struct Service {
    sessions: HashMap<SessionId, Battlefield>
}


impl Service {
    pub fn new() -> Service {
        Service { sessions: HashMap::new() }
    }

    pub fn register_session(&mut self, req: &RegisterRequest, session_id: SessionId) -> Result<(i32, SessionId), i32> {
        Ok((200, session_id))
    }
}

fn create_session_id(request: &RegisterRequest) -> SessionId {
    let now = SystemTime::now();
    let mut hasher = DefaultHasher::new();
    now.hash(&mut hasher);
    hasher.finish()
}

fn create_battlefield(request: &RegisterRequest) -> Battlefield {
    let map = Box::new(GeoMap::new(15, 15));
    Battlefield::new(map, 3)
}

fn register_response(code: i32, session_id: SessionId) -> RegisterResponse {
    let mut response = RegisterResponse::new();
    let mut status = Status::new();
    status.set_code(code);
    response.set_status(status);
    response.set_session_id(session_id);
    response
}

impl FightEngineService for Service {
    fn register(&mut self, ctx: ::grpcio::RpcContext, req: RegisterRequest, sink: ::grpcio::UnarySink<RegisterResponse>) {
        let session_id = create_session_id(&req);
        let battlefield = create_battlefield(&req);
        let registration_result = self.register_session(&req, session_id);
        sink.success(register_response(200, session_id));
    }
}

