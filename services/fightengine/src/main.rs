#[macro_use] extern crate bitflags;
#[macro_use] extern crate slog;
#[macro_use] extern crate clap;
extern crate slog_json;
extern crate rand;
extern crate futures;
extern crate grpcio;
extern crate bliss_protos;
extern crate simple_signal;
mod bliss;
mod grpc;

use clap::{App, Arg};
use futures::{Future};
use grpcio::{Environment, ServerBuilder};
use slog::{Drain, Logger};
use slog_json::{Json};
use std::sync::{Arc, Mutex, Condvar};
use std::env;
use simple_signal::{Signal};

use grpc::service::{Service};


fn arg(cmd: Option<String>, env: Result<String, env::VarError>, def: &'static str) -> String {
    cmd.unwrap_or(env.unwrap_or(String::from(def)))
}

fn main() {
    let args = App::new("")
        .version("1.0")
        .about("bliss adventures fightengine")
        .arg(Arg::with_name("port").short("p").long("grpc-listening-port").value_name("PORT")
             .help("listen on the grpc port"))
        .get_matches();

    let commandline_port = args.value_of("port").map(|x| String::from(x));
    let env_port = env::var("GRPC_LISTENING_PORT");

    let port = arg(commandline_port, env_port, "1900").parse::<u16>().expect("port must be an integer");
    let root = Logger::root(Mutex::new(Json::default(std::io::stdout())).fuse(), slog::o!());

    let should_exit = Arc::new((Mutex::new(false), Condvar::new()));
    let thread_should_exit = should_exit.clone();

    simple_signal::set_handler(&[Signal::Int, Signal::Term], move |signal| {
        let &(ref lock, ref cvar) = &*thread_should_exit;
        let mut exit = lock.lock().unwrap();
        *exit = true;
        cvar.notify_one()
    });

    // Start grpc service
    let env = Arc::new(Environment::new(1));
    let service = bliss_protos::fightengine_grpc::create_fight_engine_service(Service::new());
    let mut server = ServerBuilder::new(env)
        .register_service(service)
        .bind("127.0.0.1", port)
        .build()
        .unwrap();
    server.start();
    
    for &(ref host, port) in server.bind_addrs() {
        info!(root, "Bliss Fightengine is listening on grpc interface {}:{}", host, port);
    }

    let &(ref lock, ref cvar) = &*should_exit;
    let mut exit = lock.lock().unwrap();
    while !*exit {
        exit = cvar.wait(exit).unwrap()
    }

    info!(root, "Bliss Fightengine is shutting down ...");
    server.shutdown().wait();
}
