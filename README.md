[![pipeline status](https://gitlab.com/ruuns/bliss-adventures/badges/master/pipeline.svg)](https://gitlab.com/ruuns/bliss-adventures/commits/master)

# Bliss Adventures

An experimental roleplaying game based on Dead Simple Fantasy ruleset (for exploring microservices and rust)
