extern crate protoc_grpcio;

fn main() {
    let proto_root = "src";
    let fightengine_dir = "src/bliss/fightengine";
    let google_dir = "src/google/rpc";
    
    println!("cargo:rerun-if-changed={}",proto_root);
    
    protoc_grpcio::compile_grpc_protos(
        &["google/rpc/code.proto",
          "google/rpc/status.proto"],
        &[proto_root],
        &google_dir
    ).expect("Failed to comile gRPC definitions for google api");

    protoc_grpcio::compile_grpc_protos(
        &["bliss/fightengine/fightengine.proto", 
          "bliss/fightengine/hero.proto", 
          "bliss/fightengine/geomap.proto",
          "bliss/fightengine/action.proto"],
        &[proto_root],
        &fightengine_dir
    ).expect("Failed to comile gRPC definitions for bliss fightengine api")
}
